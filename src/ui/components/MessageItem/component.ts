import Component, { tracked } from '@glimmer/component';

export default class MessageItem extends Component {
  constructor(options) {
    super(options);
  }

  @tracked('args') get message() {
    return this.args.message;
  }
}
