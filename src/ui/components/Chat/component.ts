import Component, { tracked } from '@glimmer/component';

export default class Chat extends Component {
  paginate = {};
  socket = null;
  currentRoom = 1;
  @tracked messages = [];

  constructor(options) {
    super(options);

    this.initializeSocket(this.room);

    // get paginate params
    this.getPagination().then((paginate) => {
      this.paginate = paginate;
      this.storeMessages();
    });
  }

  initializeSocket(room) {
    if (this.socket) {
      this.socket.emit('leave');
    } else {
      this.socket = io();

      this.socket.on("receiveMessage",(data) => {

        console.log(data);

        let messages = this.messages;
        messages.unshift(data.message);
        this.messages = messages;
      });
    }

    this.socket.emit('join', { roomid: room, id: 1, name: 'glimmer-user' });
  }

  async getPagination () {
    let res = await fetch(`/api/messages/page`);
    let json = await res.json();

    return json.paginate;
  }

  async storeMessages () {
    let res = await fetch(`/api/messages?room=${this.currentRoom}`);
    let json = await res.json();

    this.messages = json.messages;
  }

  @tracked('messages') get messageCount() {
    return this.messages.length || 0;
  }

  postMessage() {
    const textarea = document.getElementById('body');
    const data = {
      "user_id": 1,
      "body": textarea.value || '',
      "room": this.currentRoom
    };

    textarea.value = '';
    this.socket.emit("deliverMessage", data);
  }

  changeRoom() {
    const newRoom = document.querySelector('#rooms-select option:checked').value;

    this.initializeSocket(newRoom);

    this.currentRoom = newRoom;

    this.storeMessages();
  }
}
